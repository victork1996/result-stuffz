#pragma once

#include <wdm.h>

void* __cdecl operator new (size_t size)
{
    return ExAllocatePool2(POOL_FLAG_PAGED, size, 'Tag');
}

void __cdecl operator delete (void* ptr, size_t)
{
    return ExFreePool(ptr);
}

inline void* __CRTDECL operator new(size_t _Size,
    _Writable_bytes_(_Size) void* _Where) noexcept
{
    (void)_Size;
    return _Where;
}

inline void __CRTDECL operator delete(void*, void*) noexcept
{
    return;
}

namespace std {

    using nullptr_t = decltype(nullptr);

    template <class... _Types>
    using void_t = void;

    template <class _Ret, class = void>
    struct _Coroutine_traits {};

    template <class _Ret>
    struct _Coroutine_traits<_Ret, void_t<typename _Ret::promise_type>> {
        using promise_type = typename _Ret::promise_type;
    };

    template <class _Ret, class...>
    struct coroutine_traits : _Coroutine_traits<_Ret> {};


    template <class = void>
    struct coroutine_handle;

    template <>
    struct coroutine_handle<void> {
        constexpr coroutine_handle() noexcept = default;
        constexpr coroutine_handle(nullptr_t) noexcept {}

        coroutine_handle& operator=(nullptr_t) noexcept {
            _Ptr = nullptr;
            return *this;
        }

        constexpr void* address() const noexcept {
            return _Ptr;
        }

        static constexpr coroutine_handle from_address(void* const _Addr) noexcept { // strengthened
            coroutine_handle _Result;
            _Result._Ptr = _Addr;
            return _Result;
        }

        constexpr explicit operator bool() const noexcept {
            return _Ptr != nullptr;
        }

        bool done() const noexcept { // strengthened
            return __builtin_coro_done(_Ptr);
        }

        void operator()() const {
            __builtin_coro_resume(_Ptr);
        }

        void resume() const {
            __builtin_coro_resume(_Ptr);
        }

        void destroy() const noexcept { // strengthened
            __builtin_coro_destroy(_Ptr);
        }

    private:
        void* _Ptr = nullptr;
    };

    template <class _Promise>
    struct coroutine_handle {
        constexpr coroutine_handle() noexcept = default;
        constexpr coroutine_handle(nullptr_t) noexcept {}

        static coroutine_handle from_promise(_Promise& _Prom) noexcept { // strengthened
            const auto _Prom_ptr = const_cast<void*>(static_cast<const volatile void*>(&_Prom));
            const auto _Frame_ptr = __builtin_coro_promise(_Prom_ptr, 0, true);
            coroutine_handle _Result;
            _Result._Ptr = _Frame_ptr;
            return _Result;
        }

        coroutine_handle& operator=(nullptr_t) noexcept {
            _Ptr = nullptr;
            return *this;
        }

        constexpr void* address() const noexcept {
            return _Ptr;
        }
        static constexpr coroutine_handle from_address(void* const _Addr) noexcept { // strengthened
            coroutine_handle _Result;
            _Result._Ptr = _Addr;
            return _Result;
        }

        constexpr operator coroutine_handle<>() const noexcept {
            return coroutine_handle<>::from_address(_Ptr);
        }

        constexpr explicit operator bool() const noexcept {
            return _Ptr != nullptr;
        }

        bool done() const noexcept { // strengthened
            return __builtin_coro_done(_Ptr);
        }

        void operator()() const {
            __builtin_coro_resume(_Ptr);
        }

        void resume() const {
            __builtin_coro_resume(_Ptr);
        }

        void destroy() const noexcept { // strengthened
            __builtin_coro_destroy(_Ptr);
        }

        _Promise& promise() const noexcept { // strengthened
            return *reinterpret_cast<_Promise*>(__builtin_coro_promise(_Ptr, 0, false));
        }

    private:
        void* _Ptr = nullptr;
    };

    constexpr bool operator==(const coroutine_handle<> _Left, const coroutine_handle<> _Right) noexcept {
        return _Left.address() == _Right.address();
    }

    template <class _Ty>
    constexpr _Ty* addressof(_Ty& _Val) noexcept {
        return __builtin_addressof(_Val);
    }

    template <class _Ty>
    struct remove_reference {
        using type = _Ty;
        using _Const_thru_ref_type = const _Ty;
    };

    template <class _Ty>
    struct remove_reference<_Ty&> {
        using type = _Ty;
        using _Const_thru_ref_type = const _Ty&;
    };

    template <class _Ty>
    struct remove_reference<_Ty&&> {
        using type = _Ty;
        using _Const_thru_ref_type = const _Ty&&;
    };

    template <class _Ty>
    using remove_reference_t = typename remove_reference<_Ty>::type;

    template <class _Ty>
    constexpr remove_reference_t<_Ty>&& move(_Ty&& _Arg) noexcept { // forward _Arg as movable
        return static_cast<remove_reference_t<_Ty>&&>(_Arg);
    }


    template <class _Ty>
    constexpr _Ty&& forward(
        remove_reference_t<_Ty>& _Arg) noexcept { // forward an lvalue as either an lvalue or an rvalue
        return static_cast<_Ty&&>(_Arg);
    }

    template <class _Ty>
    constexpr _Ty&& forward(remove_reference_t<_Ty>&& _Arg) noexcept { // forward an rvalue as an rvalue
        static_assert(!is_lvalue_reference_v<_Ty>, "bad forward call");
        return static_cast<_Ty&&>(_Arg);
    }


    struct suspend_never {
        constexpr bool await_ready() const noexcept {
            return true;
        }

        constexpr void await_suspend(std::coroutine_handle<>) const noexcept {}
        constexpr void await_resume() const noexcept {}
    };


    template <class _Ty, class = void>
    struct _Add_reference { // add reference (non-referenceable type)
        using _Lvalue = _Ty;
        using _Rvalue = _Ty;
    };

    template <class _Ty>
    struct _Add_reference<_Ty, void_t<_Ty&>> { // (referenceable type)
        using _Lvalue = _Ty&;
        using _Rvalue = _Ty&&;
    };

    template <class _Ty>
    struct add_lvalue_reference {
        using type = typename _Add_reference<_Ty>::_Lvalue;
    };

    template <class _Ty>
    using add_lvalue_reference_t = typename _Add_reference<_Ty>::_Lvalue;

    template <class _Ty>
    struct add_rvalue_reference {
        using type = typename _Add_reference<_Ty>::_Rvalue;
    };

    template <class _Ty>
    using add_rvalue_reference_t = typename _Add_reference<_Ty>::_Rvalue;

    template <class _Ty>
    add_rvalue_reference_t<_Ty> declval() noexcept {
        static_assert(_Always_false<_Ty>, "Calling declval is ill-formed, see N4892 [declval]/2.");
    }

    template <class _Ty, class... _Types,
        class = void_t<decltype(::new (declval<void*>()) _Ty(declval<_Types>()...))>>
        constexpr _Ty* construct_at(_Ty* const _Location, _Types&&... _Args) noexcept(
            noexcept(::new (_Location) _Ty(forward<_Types>(_Args)...))) /* strengthened */ {
        return ::new (_Location) _Ty(forward<_Types>(_Args)...);
    }

    template <class _Ty>
    constexpr void destroy_at(_Ty* const _Location) noexcept /* strengthened */ {
        _Location->~_Ty();
    }

    template <class _Ty>
    constexpr _Ty* launder(_Ty* _Ptr) noexcept {
        return __builtin_launder(_Ptr);
    }

} // namespace std
