#include <wdm.h>

#include "Result.h"


Result<int> duplicate(int value)
{
    return value;
}

Result<int> bar()
{
    co_return ok(1337);
}

Result<int> foo()
{
    // let's try 2 TRY's in 1 line.
    int value = TRY duplicate(TRY bar());
    co_return value;
}

Result<int> fail()
{
    return err(STATUS_UNSUCCESSFUL);
}

Result<int> foo2()
{
    int value = TRY fail();
    DbgPrint("Got To an unintended place!");
    co_return value;
}

void NTAPI DriverUnload(PDRIVER_OBJECT)
{}

extern"C" NTSTATUS NTAPI DriverEntry(PDRIVER_OBJECT obj, PUNICODE_STRING)
{
    obj->DriverUnload = &DriverUnload;

    auto fooResult = foo();
    DbgPrint("value of 'foo' is: %d\n", fooResult.value());

    auto foo2Result = foo2();
    DbgPrint("error of 'foo2' is: 0x%X\n", foo2Result.error());
    return STATUS_SUCCESS;
}