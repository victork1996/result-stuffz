#pragma once

#include "Std.h"
#include "Optional.h"


template <typename T>
struct Ok
{
    T value;
};

template <typename T>
struct Err
{
    T error;
};

template <typename T>
Ok<T> ok(T&& value)
{
    return { std::move(value) };
}

template <typename T>
Err<T> err(T&& error)
{
    return { std::move(error) };
}

template <typename Value, typename Error = NTSTATUS>
class Result
{
public:
    class promise_type;

    template <typename U = Value>
    Result(Ok<U>&& value)
        : m_hasValue(true)
        , m_value(std::move(value.value))
    {}

    template <typename U = Error>
    Result(Err<U>&& error)
        : m_hasValue(false)
        , m_error(std::move(error.error))
    {}

    Result(const Value& value)
        : m_hasValue(true)
        , m_value(value)
    {}

    Result(Value&& value)
        : m_hasValue(true)
        , m_value(std::move(value))
    {}

    Result(Result&& other)
        : m_hasValue(other.m_hasValue)
    {
        if (m_hasValue) {
            std::construct_at(std::addressof(m_value), std::move(other.m_value));
        } else {
            std::construct_at(std::addressof(m_error), std::move(other.m_error));
        }

        other.m_hasValue = false;
    }

    Result(const Result& other)
        : m_hasValue(other.m_hasValue)
    {
        if (m_hasValue) {
            std::construct_at(std::addressof(m_value), other.m_value);
        }
        else {
            std::construct_at(std::addressof(m_error), other.m_error);
        }
    }

    ~Result()
    {
        if (m_hasValue) {
            std::destroy_at(std::addressof(m_value));
        } else {
            std::destroy_at(std::addressof(m_error));
        }
    }

    bool hasValue() const
    {
        return m_hasValue;
    }

    Value& value()
    {
        return m_value;
    }

    const Value& value() const
    {
        return m_value;
    }

    Error& error()
    {
        return m_error;
    }

    const Error& error() const
    {
        return m_error;
    }

private:
    union {
        Value m_value;
        Error m_error;
    };

    bool m_hasValue;
};

template <typename Value, typename Error>
struct PromiseReturn 
{
    Optional<Result<Value, Error>> storage_{};
    Result<Value, Error>::promise_type* promise_{};

    PromiseReturn(Result<Value, Error>::promise_type& promise) noexcept
        : promise_(&promise) 
    {
        promise_->value_ = &storage_;
    }

    PromiseReturn(PromiseReturn&& that) noexcept
        : PromiseReturn{ *that.promise_ } 
    {}

    ~PromiseReturn() = default;

    operator Result<Value, Error>()
    {
        return std::move(storage_.value());
    }
};

template <typename Value, typename Error>
class Result<Value, Error>::promise_type 
{
public:
    Optional<Result<Value, Error>>* value_ = nullptr;
    promise_type() = default;
    promise_type(promise_type const&) = delete;

    PromiseReturn<Value, Error> get_return_object() noexcept
    {
        return *this;
    }

    std::suspend_never initial_suspend() const noexcept
    { 
        return {};
    }

    std::suspend_never final_suspend() const noexcept 
    { 
        return {};
    }

    template <typename U = Value>
    void return_value(U&& u) 
    {
        value_->empalce(static_cast<U&&>(u));
    }
};

template <typename Value, typename Error>
struct Awaitable 
{
    Result<Value, Error> o_;

    explicit Awaitable(Result<Value, Error> o) 
        : o_(std::move(o))
    {}

    bool await_ready() const noexcept 
    { 
        return o_.hasValue(); 
    }

    Value await_resume() 
    { 
        return std::move(o_.value());
    }

    // Explicitly only allow suspension into a Promise
    void await_suspend(std::coroutine_handle<typename Result<Value, Error>::promise_type> h) 
    {
        h.promise().value_->value().error() = o_.error();
        // Abort the rest of the coroutine. resume() is not going to be called
        h.destroy();
    }
};

template <typename Value, typename Error>
Awaitable<Value, Error> operator co_await(Result<Value, Error> o) 
{
    return Awaitable<Value, Error>{std::move(o)};
}

#define TRY co_await