#pragma once

#include "Std.h"


template <typename T>
class Optional
{
public:
    Optional()
        : m_hasValue(false)
    {}

    Optional(const T& value)
        : m_hasValue(true)
    {
        std::construct_at(storage(), value);
    }

    Optional(T&& value)
        : m_hasValue(true)
    {
        std::construct_at(storage(), std::move(value));
    }

    Optional(Optional&& other)
        : m_hasValue(other.m_hasValue)
    {
        if (m_hasValue) {
            std::construct_at(storage(), std::move(other.value()));
        }

        other.m_hasValue = false;
    }

    Optional(const Optional& other)
        : m_hasValue(other.m_hasValue)
    {
        if (m_hasValue) {
            std::construct_at(storage(), other.value());
        }
    }

    ~Optional()
    {
        reset();
    }

    template <typename... Args>
    void empalce(Args&&... args)
    {
        std::construct_at(storage(), std::forward<Args>(args)...);
    }

    void reset()
    {
        if (m_hasValue) {
            std::destroy_at(std::addressof(value()));
        }
    }

    bool hasValue() const
    {
        return m_hasValue;
    }

    operator bool() const
    {
        return m_hasValue;
    }

    T* address()
    {
        return std::launder(storage());
    }

    const T* address() const
    {
        return std::launder(storage());
    }

    T* operator->()
    {
        return address();
    }

    const T* operator->() const
    {
        return address();
    }

    T& value()
    {
        return *address();
    }

    const T& value() const
    {
        return *address();
    }

    T& operator*()
    {
        return value();
    }

    const T& operator*() const
    {
        return value();
    }

private:
    alignas (T) char m_storage[sizeof(T)];

    T* storage()
    {
        return reinterpret_cast<T*>(&m_storage);
    }

    const T* storage() const
    {
        return reinterpret_cast<const T*>(&m_storage);
    }

    bool m_hasValue;
};

